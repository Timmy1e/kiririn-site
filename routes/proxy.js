const express = require('express');
const request = require('request');
const router = express.Router();

const TELETEXT_BASE_URL = 'http://teletekst-data.nos.nl/json/';
const TRACKING_BASE_URL = 'https://internationalparceltracking.com/api/shipment';
const REQUEST_HEADERS = {
	encoding: 'json'
};

router.get('/teletext/:page', (req, res, next) => {
	let url = TELETEXT_BASE_URL + decodeURIComponent(req.params.page);

	request.get({
		url: url,
		headers: REQUEST_HEADERS
	}, (error, response, body) => {
		if (error) {
			console.log(error, response, body);
		}
		res.send(body);
	});
});

router.get('/tracking/:url', (req, res, next) => {
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
	let url = TRACKING_BASE_URL + decodeURIComponent(req.params.url);

	request.get({
		url: url,
		headers: REQUEST_HEADERS
	}, (error, response, body) => {
		if (error) {
			console.log(error, response, body);
		}
		res.send(body);
	});

});


module.exports = router;
