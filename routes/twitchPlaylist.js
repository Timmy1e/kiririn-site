const express = require('express');
const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	res.render('twitchPlaylist', {
		route: [
			{title: 'Twitch playlist', location: '/twitchPlaylist'}
		]
	});
});

module.exports = router;
