const express = require('express');
const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	res.render('kana', {route: [{title: 'Home', location: '/'}, {title: 'Kana', location: '/kana'}]});
});

module.exports = router;
