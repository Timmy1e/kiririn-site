/*global angular, moment */
/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

angular.module('kiririn').controller('packageTracking', ['$scope', '$http', '$timeout', '$sce', ($scope, $http, $timeout, $sce) => {

	const WAIT_TIME = 300000;
	let CurrentIteration = 0;

	$scope.HUMANIZED_WAIT_TIME = moment.duration(WAIT_TIME, 'ms').humanize();
	$scope.lastCheck = null;
	$scope.errorResponse = null;
	$scope.isTracking = false;
	//$scope.trackingText = $sce.trustAsHtml('<p>Please enter parcel information.</p>');
	$scope.data = {};

	$scope.init = () => {
		$scope.startTracking();
	};

	$scope.startTracking = () => {
		if ($scope.isTracking || !($scope.trackingCode && $scope.countryCode && $scope.postalCode)) {
			return;
		}

		$scope.isTracking = true;

		window.history.replaceState(
			'',
			'',
			`//${location.host}${location.pathname}
				?trackingCode=${$scope.trackingCode}
				&countryCode=${$scope.countryCode}
				&postalCode=${$scope.postalCode}`
		);
		track(true, ++CurrentIteration);
		document.getElementById('tracking-info').scrollIntoView(true);
	};

	$scope.stopTracking = () => {
		$scope.isTracking = false;
		++CurrentIteration;
	};

	function track(checkIfValid, iteration) {
		if (!$scope.isTracking || iteration !== CurrentIteration) {
			return;
		}

		$http({
			url: `/proxy/tracking/${encodeURIComponent(
				`?barcode=${encodeURIComponent($scope.trackingCode)}` +
				`&country=${encodeURIComponent($scope.countryCode)}` +
				`&postalCode=${encodeURIComponent($scope.postalCode)}` +
				`&checkIfValid=${encodeURIComponent(checkIfValid)}` +
				`&language=en`
			)}`,
			type: 'GET',
			dataType: 'json',
			crossDomain: true
		}).then(
			(response) => { // Success
				if (checkIfValid === true && response.data.valid === true) {
					$scope.trackingClass = 'info';
					$scope.lastCheck = moment();

					track(false, iteration);
				} else if (checkIfValid === true && response.data.valid === false) {
					$scope.trackingClass = 'warning';
					$scope.lastCheck = moment();

					$timeout(() => track(checkIfValid, iteration), WAIT_TIME);
				} else if (checkIfValid === false) {
					$scope.trackingClass = 'success';
					$scope.lastCheck = moment();
					$scope.data = response.data;

					$timeout(() => track(checkIfValid, iteration), WAIT_TIME);
				}
			},
			(response) => { // Error
				console.error(response);

				$scope.trackingClass = 'danger';
				$scope.errorResponse = {
					status: response.status,
					statusText: response.statusText
				};
				$scope.lastCheck = moment();

				$timeout(() => track(checkIfValid, iteration), WAIT_TIME);
			},
			() => { // Finally
				$scope.$apply();
			}
		);
	}
}]).directive('trackingText', [() => ({
	restrict: 'E',
	scope: {
		now: '<',
		isTracking: '<',
		trackingClass: '<',
		error: '<?',
		waitTime: '@?',
		dateTimeFormat: '@?'
	},
	link: (scope, element, attrs) => {

		const WAIT_TIME = scope.waitTime || 300000;
		const DATE_TIME_FORMAT = scope.dateTimeFormat || 'lll';

		scope.HUMANIZED_WAIT_TIME = moment.duration(WAIT_TIME, 'ms').humanize();

		scope.$watch('now', () => {
			const NOW = moment(scope.now);
			if (NOW) {
				scope.humanizedLastCheck = NOW.format(DATE_TIME_FORMAT);
				scope.humanizedNextCheck = NOW.add(WAIT_TIME, 'ms').format(DATE_TIME_FORMAT);
			}
		});
	},
	templateUrl: '/tracking/trackingText.html'
})]).directive('package', [() => ({
	restrict: 'E',
	scope: {
		data: '<'
	},
	link: (scope, element, attrs) => {

		scope.humanize = (variable, type) => {
			switch (type || typeof(variable)) {
				case 'boolean':
					return variable === true ? 'Yes' : 'No';
				case 'DateString':
					return moment(variable).format('LLL');
				case 'string':
					return variable
						.replace(/([a-z])([A-Z])/g, '$1 $2')
						.replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
						.replace(/^./, str => str.toUpperCase());
				default:
					return `${variable} (Unknown type "${typeof(variable)}")`;
			}
		};
	},
	templateUrl: '/tracking/package.html'
})]);