/*global angular */
/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

angular.module('kiririn').controller('importCalculator', ['$scope', ($scope) => {
	const EUROSYM = '\u20AC';
	$scope.importCost = 0.00;

	$scope.calculateImportCost = () => {
		const VAT = 0.21;
		const TARIFF = 0.042;
		const ItemCost = saveParseFloat($scope.itemCost);
		const ShippingCost = saveParseFloat($scope.shippingCost);
		const ShippingType = saveParseInt($scope.shippingType);
		let importCost = 0;

		if (ItemCost > 0 && ShippingCost >= 0 && ShippingType > 0) {
			window.history.replaceState(
				'',
				'',
				`//${location.host}${location.pathname}
					?itemCost=${ItemCost}
					&shippingCost=${ShippingCost}
					&shippingType=${ShippingType}`
			);
		} else {
			window.history.replaceState(
				'',
				'',
				`//${location.host}${location.pathname}`
			);
		}

		if (ItemCost >= 22) {
			importCost += (ItemCost + ShippingCost) * VAT;
		}

		if (ItemCost >= 150) {
			importCost += (ItemCost + ShippingCost) * TARIFF;
		}

		if (importCost > 0) {
			if (ShippingType === 1) {
				importCost += 13.00;
			} else if (ShippingType === 2) {
				importCost += 17.50;
			}
			$scope.isCalculated = true;
		} else {
			$scope.isCalculated = false;
		}
		$scope.importCost = importCost;
		return importCost;
	};

	$scope.getImportCost = () => {
		return toCurrencyString($scope.calculateImportCost());
	};

	$scope.calculateTotalCost = () => {
		const ItemCost = saveParseFloat($scope.itemCost);
		const ShippingCost = saveParseFloat($scope.shippingCost);
		const ImportCost = saveParseFloat($scope.importCost);

		return (ItemCost + ShippingCost + ImportCost);
	};

	$scope.getTotalCost = () => {
		return toCurrencyString($scope.calculateTotalCost());
	};

	$scope.cardBorderClass = () => {
		let importCost = $scope.importCost;

		if (importCost < 20) {
			return 'info';
		} else if (importCost < 30) {
			return 'primary';
		} else if (importCost < 40) {
			return 'warning';
		} else {
			return 'danger';
		}
	};

	function saveParseFloat(value) {
		return bottom(isNaN(parseFloat(value)) ? 0 : parseFloat(value), 0);
	}

	function saveParseInt(value) {
		return bottom(isNaN(parseInt(value)) ? 0 : parseInt(value), 0);
	}

	function bottom(value, bottom) {
		return value > bottom ? value : bottom;
	}

	function toCurrencyString(value) {
		return `${EUROSYM} ${parseFloat(value).toFixed(2)}`;
	}
}]);
