/*global angular, $ */
/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

angular.module('kiririn').controller('twitchPlaylist', ['$scope', '$http', '$window', ($scope, $http, $window) => {
	const PREFIX_APPLICATION = 'xyz.kiririn.twitchplaylist';
	const PREFIX_PLAYLISTS = PREFIX_APPLICATION + '.playlists';
	const PREFIX_PLAYLIST = PREFIX_APPLICATION + '.playlist';
	const PREFIX_VIDEO = PREFIX_APPLICATION + '.video';

	const TWITCH_API_URL = 'https://api.twitch.tv/helix';
	const TWITCH_API_CLIENT = '8s2e86v3zjo4ct80s0sz5i5185ixta';

	$scope.selectedPlaylist = null;
	$scope.rawPlaylists = null;
	$scope.newPlaylist = null;
	$scope.playlists = null;
	$scope.newVideo = null;
	$scope.videos = null;

	function init() {
		let playlists = getPlaylists();

		if (!playlists) {
			playlists = [];
			savePlaylists(playlists);
		}
		$scope.rawPlaylists = playlists;

		let fullPlaylists = [];

		playlists.forEach(playlist => {
			let fullPlaylist = getPlaylist(playlist);

			if (!fullPlaylist) {
				fullPlaylist = {
					id: playlist,
					name: playlist,
					videos: []
				};
				savePlaylist(fullPlaylist);
			}

			fullPlaylists.push(fullPlaylist);
		});
		$scope.playlists = fullPlaylists;
	}

	$scope.selectedPlaylistIdInit = (array) => {
		let value = null;
		if (array) {
			if (array.length > 0) {
				value = array[0].id;
			}
		}
		return value;
	};

	$scope.$watch('selectedPlaylistId', () => {
		let selectedPlaylistId = $scope.selectedPlaylistId;
		if (!selectedPlaylistId) {
			return;
		}

		$scope.playlists.some((playlist) => {
			if (selectedPlaylistId === playlist.id) {
				$scope.selectedPlaylist = playlist;
				$scope.videos = [];
				playlist.videos.forEach((videoId) => {
					let video = $scope.getVideo($scope.selectedPlaylist.id, videoId);
					if (video) {
						$scope.videos.push(video);
					}
				});
				return false;
			}
		});
	});

	$scope.openModal = (modalId) => {
		$(modalId).modal('show');
	};

	$scope.addPlaylistModal = () => {
		$('#addPlaylistModal').modal('show');
	};

	$scope.addPlaylist = () => {
		let newPlaylistName = $scope.newPlaylist;
		let newPlaylistId;
		let foundNewPlaylistId = false;

		function rawPlaylistsSome(playlistId) {
			return playlistId === newPlaylistId;
		}

		do {
			newPlaylistId = (Math.random() + 1).toString(36).substring(2);
			foundNewPlaylistId = !newPlaylistId || $scope.rawPlaylists.some(rawPlaylistsSome);
		} while (foundNewPlaylistId);

		$scope.rawPlaylists.push(newPlaylistId);
		savePlaylists($scope.rawPlaylists);

		let newPlaylistObj = {
			id: newPlaylistId,
			name: newPlaylistName,
			videos: []
		};

		$scope.playlists.push(newPlaylistObj);
		savePlaylist(newPlaylistObj);

		$('#addPlaylistModal').modal('hide');

		delete $scope.newPlaylist;
	};

	$scope.addVideoToPlaylist = () => {
		let newVideo = $scope.newVideo;

		if (!newVideo) {
			return;
		}

		let newVideoArray = newVideo.split('/');
		let newVideoId = null;
		if (newVideoArray.length > 1) { // Probably a URL.
			for (let i = newVideoArray.length - 1; i >= 0; i--) {
				if (isANumber(newVideoArray[i])) {
					newVideoId = parseInt(newVideoArray[i]);
					break;
				}
			}
		} else if (isANumber(newVideo)) { // It's a number.
			newVideoId = parseInt(newVideo);
		} else { // Is something else.
			console.log('2c', 'PANIC');
			return;
		}

		if ($scope.videos.some(x => x.id === newVideoId)) {
			console.log('4a', 'Already in here');
			return;
		}

		delete $scope.newVideo;

		let newVideoObj = {
			id: newVideoId,
			isWatched: false
		};

		getVideoDetails(newVideoObj);

		$scope.selectedPlaylist.videos.push(newVideoId);

		savePlaylist($scope.selectedPlaylist);

		$scope.videos.push(newVideoObj);
	};

	$scope.toggleVideoWatched = (videoId) => {
		$scope.videos.some((video) => {
			if (video.id === videoId) {
				// noinspection JSUndefinedPropertyAssignment
				video.isWatched = !video.isWatched;
				saveVideo($scope.selectedPlaylist.id, video);
				return true;
			}
			return false;
		});

	};


	/* Remove playlist */
	$scope.removePlaylistModal = () => {
		if (!$scope.selectedPlaylist) {
			return;
		}

		$('#removePlaylistModal')
			.modal({
				closable: false
			})
			.modal('show');
	};

	$scope.removePlaylist = () => {
		let rawPlaylistIndex = -10;
		$scope.rawPlaylists.some((elm, inx) => {
			if (elm.id === $scope.selectedPlaylist.id) {
				rawPlaylistIndex = inx;
				return true;
			}
			return false;
		});

		let playlistsIndex = -10;
		$scope.playlists.some((elm, inx) => {
			if (elm.id === $scope.selectedPlaylist.id) {
				playlistsIndex = inx;
				return true;
			}
			return false;
		});


		$scope.rawPlaylists.splice(rawPlaylistIndex, 1);

		savePlaylists($scope.rawPlaylists);
		$scope.rawPlaylists = getPlaylists();

		$scope.playlists.splice(playlistsIndex, 1);

		removePlaylist($scope.selectedPlaylist.id);

		$scope.selectedPlaylist.videos.forEach((elm) => {
			console.log(elm);
			removeVideo($scope.selectedPlaylist.id, elm);
		});

		$scope.selectedPlaylist = null;
		$scope.selectedPlaylistId = null;
		$('#removePlaylistModal').modal('hide');
	};


	/* Remove video */
	$scope.removeVideoModal = (videoId) => {
		$scope.videos.some((elm, ind) => {
			if (videoId === elm.id) {
				$scope.videoToRemove = elm;
				return true;
			}
		});
		$('#removeVideoModal')
			.modal({
				closable: false,
				onDeny: function () {
					delete $scope.videoToRemove;
				}
			})
			.modal('show');
	};

	$scope.removeVideo = () => {
		let videoIndex = -10;
		$scope.videos.some((elm, ind) => {
			if ($scope.videoToRemove.id === elm.id) {
				videoIndex = ind;
				return true;
			}
		});

		let playlistIndex = -10;
		$scope.selectedPlaylist.videos.some((elm, ind) => {
			if ($scope.videoToRemove.id === elm) {
				playlistIndex = ind;
				return true;
			}
		});

		$scope.videos.splice(videoIndex, 1);

		removeVideo($scope.selectedPlaylist.id, $scope.videoToRemove.id);

		$scope.selectedPlaylist.videos.splice(playlistIndex, 1);

		savePlaylist($scope.selectedPlaylist);

		delete $scope.videoToRemove;
		$('#removeVideoModal').modal('hide');
	};


	/* Thumbnail */
	$scope.getThumbnail = (video) => {
		if (!video.cache) {
			return;
		}

		if (video.cache.thumbnail_url) {
			return video.cache.thumbnail_url.replace(/%{width}/, 125).replace(/%{height}/, 70);
		} else if (video.cache.thumbnails && video.cache.thumbnails.large) {
			return _getPriorityThumbnail(video.cache.thumbnails.large);
		} else if (video.cache.thumbnails && video.cache.thumbnails.medium) {
			return _getPriorityThumbnail(video.cache.thumbnails.medium);
		} else if (video.cache.thumbnails && video.cache.thumbnails.small) {
			return _getPriorityThumbnail(video.cache.thumbnails.small);
		} else {
			return null;
		}

		/**
		 * Get old thumbnail.
		 * @deprecated
		 * @param thumbnails
		 * @return {*}
		 */
		function _getPriorityThumbnail(thumbnails) {
			const TYPE_UPLOADED = 'uploaded';
			const TYPE_GENERATED = 'generated';

			let thumbnailValue = null;

			thumbnails.sort((a, b) => {

				if (a.type === b.type) {
					return 0;
				}

				if (
					(a.type === TYPE_UPLOADED && b.type !== TYPE_UPLOADED) ||
					(a.type === TYPE_GENERATED && b.type !== TYPE_GENERATED && b.type !== TYPE_UPLOADED)
				) {
					return 1;
				}
				return -1;
			}).some((thumbnail) => {
				thumbnailValue = thumbnail.url;
				return true;
			});
			return thumbnailValue;
		}
	};

	function savePlaylists(playlistsToSave) {
		return saveValue(PREFIX_PLAYLISTS, playlistsToSave);
	}

	function savePlaylist(playlistToSave) {
		delete playlistToSave.$$hashKey;
		return saveValue(PREFIX_PLAYLIST + '_' + playlistToSave.id, playlistToSave);
	}

	function saveVideo(playlistId, videoToSave) {
		delete videoToSave.$$hashKey;
		return saveValue(PREFIX_VIDEO + '_' + playlistId + '_' + videoToSave.id, videoToSave);
	}

	function saveValue(key, value) {
		return $window.localStorage.setItem(key, JSON.stringify(value));
	}

	function getPlaylists() {
		return getValue(PREFIX_PLAYLISTS);
	}

	function getPlaylist(playlistId) {
		return getValue(PREFIX_PLAYLIST + '_' + playlistId);
	}

	function getVideo(playlistId, videoId) {
		return getValue(PREFIX_VIDEO + '_' + playlistId + '_' + videoId);
	}

	$scope.getVideo = getVideo;

	function getValue(key) {
		let value = $window.localStorage.getItem(key);

		return !value ? null : JSON.parse(value);
	}

	function removeVideo(playlistId, videoId) {
		return removeValue(PREFIX_VIDEO + '_' + playlistId + '_' + videoId);
	}

	function removePlaylist(playlistId) {
		return removeValue(PREFIX_PLAYLIST + '_' + playlistId);
	}

	function removeValue(key) {
		return $window.localStorage.removeItem(key);
	}

	function getVideoDetails(video) {
		$http({
			url: TWITCH_API_URL + '/videos/?id=' + video.id,
			type: 'GET',
			dataType: 'json',
			headers: {
				'Client-ID': TWITCH_API_CLIENT
			},
			crossDomain: true
		}).then(
			function (response) { // Success
				console.log('getVideoDetails', response);
				video.cache = response.data.data[0];
				saveVideo($scope.selectedPlaylist.id, video);
			},
			function (response) { // Error
				console.error(response);
			},
			function () { // Finally
				$scope.$apply();
			}
		);
	}

	function isANumber(value) {
		if (!value) {
			return false;
		}

		return !isNaN(parseInt(value)) && isFinite(value);
	}

	// Loaded all script, now start.
	init();
}]).directive('abEnter', () => {
	return (scope, element, attrs) => {
		element.bind('keydown keypress', (event) => {
			if (event.which === 13) {
				scope.$apply(() => {
					scope.$eval(attrs.abEnter);
				});

				event.preventDefault();
			}
		});
	};
});
